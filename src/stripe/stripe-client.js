const secrets = require('../../secrets');
const stripe = require('stripe')(secrets.stripeTokens.live.secret);

const stripeOnRequest = (req) => {
    return req;
};
const stripeOnResponse = (res) => {
    return res;
};

const listen = (doListen) => {
    let fn = doListen ? stripe.on : stripe.off;
    fn('request', stripeOnRequest);
    fn('response', stripeOnResponse);
};

const configure = () => {
    stripe.setTimeout(50000);
    stripe.setApiVersion('2018-09-24');
    listen(true);
};

configure();

module.exports = stripe;