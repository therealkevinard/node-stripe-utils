/**
 * Almost-useless wrapper for promise.all
 * @param promises
 * @return {Promise}
 */
const collectAllPromises = async (promises) => {
    return new Promise(resolve => {
        Promise.all(promises)
            .then(resolved => {
                resolve(resolved)
            })
    })
};

/**
 * rewrite of _.chunk
 * @param chunk
 * @param chunkSize
 * @return {Array}
 */
const getArrayChunks = (chunk, chunkSize = 100) => {
    let i, j;
    let chunks = [];
    for (i = 0, j = chunk.length; i < j; i += chunkSize) {
        let slice = chunk.slice(i, i + chunkSize);
        chunks.push(slice);
    }
    return chunks;
};

/**
 * Create Roman numeral from int year
 * @param year
 * @return {*}
 */
const romanize = (year) => {
    if (!+year)
        return NaN;
    const digits = String(+year).split("");
    const key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
        "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"];
    let roman = "";
    let i = 3;
    while (i--)
        roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
};

module.exports = {
    collectAllPromises,
    getArrayChunks,
    romanize
};