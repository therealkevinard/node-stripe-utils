const secrets = require('../../secrets');
const queries = require('../database/commonQueries');
const {queryWrapper} = require('../database');
const {collectAllPromises} = require('../utils');

//----------------------------------------------- Extractors
/**
 * Pull and hydrate posts from wpdb.
 * Resolves with []{
 *     author: {
 *       name,
 *       bio
 *     },
 *     post: {
 *      // raw record from sql query
 *     },
 *     postmeta: []{
 *       key,
 *       value
 *     }
 *     terms: []{
 *      // raw record from wp meta query
 *     }
 * }
 * @param staging
 * @param post_type
 * @return {Promise<*>}
 */
const fetchPosts = async (staging, post_type) => {
    let posts = await queryWrapper(
        queries.getPublishedContentByType('wp', post_type),
        staging
    );

    //-- add post meta to posts
    let prom_postMeta = posts.map(async post => {
        return new Promise(async postResolve => {
            let meta = await queryWrapper(queries.getPostMetaRowsForId('wp', post.ID), staging);
            let postmeta = meta.map(m => {
                return {
                    key: m.meta_key,
                    value: m.meta_value,
                }
            });

            postResolve({
                post,
                postmeta
            })
        })
    });
    posts = await collectAllPromises(prom_postMeta);
    //-- add taxonomies
    let prom_postTax = posts.map(async post => {
        return new Promise(async taxResolve => {
            let postTax = await staging.query(
                    `select termtax.taxonomy as term_type, terms.term_id as term_id, terms.name as term_name, terms.slug as term_slug
                     from wp_term_relationships relate
                            left join wp_terms terms on relate.term_taxonomy_id = terms.term_id
                            left join wp_term_taxonomy termtax on relate.term_taxonomy_id = termtax.term_taxonomy_id
                     where relate.object_id = ?`,
                [post.post.ID],
                (err, results) => {
                    if (err) throw err;
                    let res = {
                        ...post,
                        terms: results
                    };
                    taxResolve(res)
                }
            )
        })
    });
    posts = await collectAllPromises(prom_postTax);
    posts = posts.map(p => {
        let author = p.postmeta.filter(m => {
            return m.key === 'Author'
        });
        let bio = p.postmeta.filter(m => {
            return m.key === 'Author Bio, Brief'
        });

        let authorOb = {};
        author[0] ? authorOb.name = author[0].value : null;
        bio[0] ? authorOb.bio = bio[0].value : null;

        return {
            ...p,
            author: authorOb
        }
    });

    //-- done. return
    return posts;
};
const restFetchPost = async (postId) => {
    return new Promise(resolve => {

    })
};
const fetchUsers = async (staging) => {
    let users = await queryWrapper(`SELECT *
                                    from wp_users`, staging);
    let userMeta = await queryWrapper(`select *
                                       from wp_usermeta`, staging);
    let pmproRecords = await queryWrapper(`select *
                                           from wp_pmpro_memberships_users`, staging);
    let pmproPayments = await queryWrapper(`select *
                                            from wp_pmpro_membership_orders`, staging);

    return users.map(u => {
        let user_metadata = {};
        userMeta
            .filter(m => {
                return m.user_id === u.ID;
            })
            .map(m => {
                user_metadata[m.meta_key] = m.meta_value;
            });

        let user_mshpdata = {
            memberships: [],
            payments: []
        };

        user_mshpdata.memberships = pmproRecords
            .filter(m => {
                return m.user_id === u.ID;
            });

        user_mshpdata.payments = pmproPayments
            .filter(p => {
                return p.user_id === u.ID;
            });

        return {
            userEntity: u,
            userMetadata: user_metadata,
            userMemberdata: user_mshpdata
        };
    })
};

//----------------------------------------------- Parse/Transform

//----------------------------------------------- Loaders

//-----------------------------------------------
module.exports = {
    fetchPosts, restFetchPost, fetchUsers
};