const html2md = require('html-markdown');
const {collectAllPromises} = require('../utils');
const {formatMysqlTimestamp} = require('../database');

//----------------------------------------------- Extractors
const fetchTerms = async (localdev) => {
    return new Promise(resolve => {
        let q = `select id, name, slug
                 from rainlab_blog_categories`;
        localdev.query(
            q,
            (err, results) => {
                if (err) throw err;
                resolve(results);
            }
        )
    });
};
const fetchIssues = async (localdev) => {
    return new Promise(resolve => {
        let q = `select *
                 from trka_tsrliteraryextensions_issues`;
        localdev.query(
            q,
            (err, results) => {
                if (err) throw err;
                resolve(results);
            }
        )
    });
};
const fetchAuthors = async (localdev) => {
    return new Promise(resolve => {
        let q = `select *
                 from trka_tsrliteraryextensions_authors`;
        localdev.query(
            q,
            (err, results) => {
                if (err) throw err;
                resolve(results);
            }
        )
    });
};
const fetchPosts = async (localdev) => {
    return new Promise(resolve => {
        let q = `select *
                 from rainlab_blog_posts`;
        localdev.query(
            q,
            (err, results) => {
                if (err) throw err;
                resolve(results);
            }
        )
    });
};
const fetchUsers = async (localdev) => {
    return new Promise(resolve => {
        let q = `select *
                 from users`;
        localdev.query(
            q,
            (err, results) => {
                if (err) throw err;
                resolve(results);
            }
        )
    })
};

//----------------------------------------------- Loaders
const insertTerms = async (localdev, terms) => {
    return new Promise(async resolve => {
        let tplInsert = `INSERT into rainlab_blog_categories (name, slug, parent_id, nest_left, nest_right, nest_depth, created_at, updated_at)
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;
        let prom_termInserts = terms.category.map(cat => {
            return new Promise(termResolve => {
                let now = formatMysqlTimestamp();
                let tplValues = [
                    cat.term_name, cat.term_slug,
                    1, 1, 2, 1,
                    now, now
                ];
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) throw err;
                        termResolve({
                            results,
                            fields
                        })
                    }
                )
            });
        });
        let results = await collectAllPromises(prom_termInserts);
        resolve(results);
    })
};
const insertIssues = async (localdev, issues) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into trka_tsrliteraryextensions_issues (name, volume, number, season, year, slug, featured_articles, published_at, current_issue, published, navigation)
                         values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
        let now = formatMysqlTimestamp();
        let prom_issueInserts = issues.map(issue => {
            return new Promise(issueResolve => {
                let tplValues = [
                    issue.name,
                    issue.volume,
                    issue.number,
                    issue.season,
                    issue.year,
                    `${issue.season.toLowerCase()}-${issue.year}`,
                    "[]", now, 0, 1, "[]"
                ];
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) throw err;
                        issueResolve({
                            results,
                            fields
                        })
                    }
                )
            })
        });
        let results = await collectAllPromises(prom_issueInserts);
        resolve(results);
    })
};
const insertAuthors = async (localdev, authors) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into trka_tsrliteraryextensions_authors (name, bio_html)
                         VALUES (?, ?)`;
        let prom_authorInserts = authors.map(author => {
            return new Promise(authorResolve => {
                let tplValues = [
                    author.name || '',
                    author.bio ? `<p>${author.bio}</p>` : ''
                ];
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) throw err;
                        authorResolve({
                            results,
                            fields
                        })
                    }
                )
            })
        });
        let results = await collectAllPromises(prom_authorInserts);
        resolve(results)
    })
};
const insertPosts = async (localdev, posts) => {
    return new Promise(async resolve => {
        let tplInsert = `INSERT into rainlab_blog_posts (user_id, title, slug, excerpt, content, content_html, published_at, published, created_at, updated_at, author_id, typo_style, member_content, issue_id)
                         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); `;
        //----
        let prom_postInserts = posts.map(post => {
            let now = formatMysqlTimestamp();
            return new Promise(postResolve => {
                let convertedBreaks = post.post.post_content.replace(/(?:\r\n|\r|\n)/g, '<br>');
                post.post.post_content = convertedBreaks;

                let mdComplete = html2md.html2mdFromString(post.post.post_content);

                let tplValues = [
                    1,
                    post.post.post_title, post.post.post_name,
                    post.post.post_excerpt, post.post.post_content, post.post.post_content,
                    now, 1, now, now,
                    post.post_contributor_id || 0,
                    "",
                    (post.post_category_ids && post.post_category_ids.indexOf(36)) ? 1 : 0, // id: 36 maps to the imported member_content category
                    post.post_issue_id || 0,
                ];
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) {
                            throw err;
                        }
                        postResolve({
                            results,
                            fields
                        })
                    }
                )
            })
        });
        let results = await collectAllPromises(prom_postInserts);
        resolve(results);
    })
};
/**
 *
 * @param localdev
 * @param pstCatMap
 *  []{postId, categoryId}
 * @return {Promise}
 */
const loadPostCatMap = async (localdev, postCatMap) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into rainlab_blog_posts_categories (post_id, category_id)
                         VALUES (?, ?)`;
        let postCatMapInserts = [];

        postCatMap.forEach(pcm=>{
            postCatMapInserts.push([pcm.postId, pcm.categoryId]);
        });

        let prom_postcatInserts = postCatMapInserts.map(postCatMap => {
            return new Promise(postcatResolve => {
                let tplValues = postCatMap;
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) console.log(err);
                        postcatResolve(results);
                    }
                )
            })
        });
        let results = await collectAllPromises(prom_postcatInserts);
        resolve(results);
    })
};
const insertUsers = async (localdev, users) => {
    return new Promise(async resolve => {
        let tplInsert = `insert into users (name, surname, email, password, is_activated, address_1, address_2, city, state_region, postcode)
                         values (?, ?, ?, "---", 1, ?, ?, ?, ?, ?) `;
        let prom_userInserts = users.map(u => {
            return new Promise(userResolve => {
                let tplValues = [];
                localdev.query(
                    tplInsert,
                    tplValues,
                    (err, results, fields) => {
                        if (err) throw err;
                        userResolve({
                            results,
                            fields
                        })
                    }
                )
            })
        });
        let results = await collectAllPromises(prom_userInserts);
        resolve(results);
    })
};

//-----------------------------------------------
module.exports = {
    fetchTerms, fetchIssues, fetchPosts, fetchAuthors, fetchUsers,
    insertTerms, insertPosts, loadPostCatMap, insertAuthors, insertIssues, insertUsers
};